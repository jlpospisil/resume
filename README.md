## Resume - Josh Pospisil
This is a sample monorepo to demonstrate my front-end capabilities in a way that also allows me to maintain and serve an html version of my resume.  That resume is being automatically updated and deployed using Gitlab's CI/CD tools and is served via Gitlab Pages.  It can be viewed [here](https://jlpospisil.gitlab.io/resume/).

### Overview
The monorepo itself is utilizing Lerna, and all packages within use Typescript and React.  I chose to use a monorepo here somewhat untraditionaly due to the fact that this is for demonstration purposes only.  I wouldn't typically include the actual applications in a monorepo.  I would primarily use a monorepo for a style-guide, utilities, etc.  This allowed me to keep all of these items together and let me utilize the style-guide during deployment without publishing any artifacts to a private or public NPM repository.  

##### Packages
* **style-guide:** This package creates a reusable component library.  It is utilizing Storybook to provide a UI for viewing and interacting with said components during development.
* **resume:** This package contains my resume.  It is using the components from the style-guide to do so and also provides a very small amount of translation (spanish only) to demonstrate knowledge/capabilities accounting for internationalization.
* **example-app:** This package is admittedly terrible.  It's only purpose is to demonstrate the reusable nature of the style-guide components.  It merely pukes a bunch of those components onto a single page.

### Installation
    npm run bootstrap
    
### Running storybook
    npm run serve:style-guide
    
### Running resume app
    npm run serve:resume

### Notes
There is an issue caused by `npm link`ing the style-guide.  To avoid "duplicate" instances of react, we remove the react module from any app that uses style-guid and symlink it to the version in the style-guid.  This is done automatically after the bootstrap but could be run independently as well.

    npm run post-bootstrap:resume

This is likely the issue if you see the following error when trying to serve an app:

    Invalid hook call. Hooks can only be called inside of the body of a function component. This could happen for one of the following reasons:
    1. You might have mismatching versions of React and the renderer (such as React DOM)
    2. You might be breaking the Rules of Hooks
    3. You might have more than one copy of React in the same app
    See https://fb.me/react-invalid-hook-call for tips about how to debug and fix this problem.
