import * as React from 'react';
import { render } from '@testing-library/react';
import App from './App';

describe('App', () => {
  it('renders', () => {
    const { getByTestId } = render(<App />);
    expect(getByTestId('resume')).toBeInTheDocument();
  });

  describe('header', () => {
    it('renders', () => {
      const { getByTestId } = render(<App />);
      expect(getByTestId('resume-header')).toBeInTheDocument();
    });
  });

  describe('content', () => {
    it('renders', () => {
      const { getByTestId } = render(<App />);
      expect(getByTestId('resume-content')).toBeInTheDocument();
    });
  });
});
