import i18n from 'i18next';
import en from './en';
import es from './es';
import LanguageDetector from 'i18next-browser-languagedetector';

i18n
  .use(LanguageDetector)
  .init({
    debug: process.env.NODE_ENV !== 'production',
    detection: {
      order: ['navigator', 'cookie', 'localStorage', 'querystring', 'htmlTag'],
    },
    fallbackLng: 'en',
    resources: {
      en: { translation: en },
      es: { translation: es }
    }
  });

export default i18n;
