import { Resume } from './types';

const data: Resume = {
  headings: {
    experience: 'Experiencia',
    skills: 'Habilidades',
    education: 'Educación',
  },
  labels: {
    present: 'Presente',
  },
  contactInformation: {
    name: 'Josh Pospisil',
    title: 'Ingeniero de software',
  },
  about: 'Ingeniero experimentado de pila completa con una historia demostrada trabajando en la industria de servicios y tecnología de la información. Experto en marcos de JavaScript modernos (Vue, React, React Native), así como tecnologías de back-end que incluyen, entre otros, Spring Boot (Java) y Django (Python).',
};

export default data;
