export interface ResumeHeadings {
  experience: string,
  skills: string,
  education: string,
}

export interface ContactInformation {
  name: string,
  title?: string,
  email?: string,
  phoneNumber?: string,
  address?: string,
}

export interface Skill {
  skill: string,
  rating?: number,
  scale?: number,
}

export interface ResumeLabels {
  present: string,
}

export interface ResumeExperienceBulletItem {
  item: string,
  children?: Array<string>,
}

export interface ResumeExperienceItem {
  company: string,
  title: string,
  startDate: string,
  endDate?: string,
  bulletItems?: Array<ResumeExperienceBulletItem>
}

export interface ResumeEducation {
  degree: string,
  school: string,
  location: string,
  gpa: string,
  graduationDate: string,
}

export interface Resume {
  headings: ResumeHeadings,
  labels: ResumeLabels,
  contactInformation: ContactInformation,
  about?: string,
  skills?: Array<Skill>,
  experience?: Array<ResumeExperienceItem>,
  education?: ResumeEducation
}
