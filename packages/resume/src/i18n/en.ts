import { Resume } from './types';

const data: Resume = {
  headings: {
    experience: 'Experience',
    skills: 'Skills',
    education: 'Education',
  },
  labels: {
    present: 'Present',
  },
  contactInformation: {
    name: 'Josh Pospisil',
    title: 'Software Engineer',
    email: 'josh.pospisil@gmail.com',
    phoneNumber: '(402) 516-4440',
    address: 'Omaha, NE',
  },
  about: 'Experienced full stack engineer with a demonstrated history working in the information technology and services industry. Skilled in modern JavaScript frameworks (Vue, React, React Native) as well as back end technologies including, but not limited to, Spring Boot (Java) and Django (Python).',
  skills: [
    { skill: 'AWS', rating: 4, scale: 5 },
    { skill: 'GCP', rating: 4, scale: 5 },
    { skill: 'JavaScript', rating: 5, scale: 5 },
    { skill: 'Vue.js', rating: 5, scale: 5 },
    { skill: 'React', rating: 4, scale: 5 },
    { skill: 'Java', rating: 4, scale: 5 },
    { skill: 'Spring Boot', rating: 4, scale: 5 },
    { skill: 'Python', rating: 4, scale: 5 },
    { skill: 'SQL', rating: 4, scale: 5 },
    { skill: 'RabbitMQ', rating: 3, scale: 5 },
    { skill: 'Kafka', rating: 3, scale: 5 },
    { skill: 'HTML', rating: 5, scale: 5 },
    { skill: 'CSS', rating: 5, scale: 5 },
    { skill: 'Git', rating: 4, scale: 5 },
    { skill: 'REST', rating: 5, scale: 5 },
  ],
  education: {
    degree: 'Bachelor of Science degree in Systems and Network Administration',
    school: 'Bellevue University',
    location: 'Bellevue, NE',
    gpa: '4.0',
    graduationDate: 'Dec 2010',
  },
  experience: [
    {
      company: 'Tango Card',
      title: 'Senior Software Engineer',
      startDate: 'April 2019',
      bulletItems: [
        { item: 'Worked as part of a team to design, develop, and maintain a suite of micro-service driven applications using technologies such as JavaScript (Vue), Java (Spring Boot), PostgreSQL, Redis, RabbitMQ, and Elasticsearch' },
        { item: 'Worked as part of a team to design, develop, and maintain pipelines that allowed for continuous deployment of all services and applications' },
        { item: 'Built, tested, and deployed applications leveraging cloud technologies provided by AWS including EC2, RDS, SQS, Kinesis, Lambda, S3' },
        { item: 'Ensured success of automated pipelines by adding and maintaining test coverage on code including unit, integration, and UI (end-to-end) tests' },
        { item: 'Participated in functional and technical designs, leading several including one for creating an application to provide an open loop payment product.' },
        { item: 'Participated in code reviews to ensure consistency and quality across applications.' },
        { item: 'Worked closely with the product and design teams in order to lead a team of 5-8 software engineers.' }
      ],
    },
    {
      company: 'CSG Actuarial, LLC ',
      title: 'Software Engineer',
      startDate: 'Nov 2018',
      endDate: 'April 2019',
      bulletItems: [
        { item: 'Built, tested, and deployed applications leveraging cloud technologies provided by Google Cloud Platform' },
        { item: 'Built, tested, and deployed scalable, RESTful APIs to facilitate application functionality' },
        {
          item: 'Migrated software to current technology platforms on both the front end and the back end.',
          children: [
            'On the front end, Integrated Vue.js into existing applications to improve functionality, usability, and speed',
            'Migrated back end from a home-built platform that was dependent on Python 2.7 and webapp2 to current versions of Python and Django',
          ],
        },
      ],
    },
    {
      company: 'Midwest Laboratories, Inc. ',
      title: 'Software Engineer',
      startDate: 'Sept 2010',
      endDate: 'Nov 2018',
      bulletItems: [
        { item: 'Participated in all stages of development life cycle (planning, analysis, design, implementation, and maintenance)' },
        { item: 'Developed and maintained applications to fit the needs of the business' },
        { item: 'Developed applications using modern web technologies and frameworks including HTML5, CSS, SASS, Node.js, React, Vue.js, and Bootstrap' },
        { item: 'Developed and maintained RESTful APIs to facilitate communication between internal applications as well as 3rd party applications' },
        { item: 'Developed and maintained methods of communicating with external web services via SOAP and REST' },
        { item: 'Implemented version control system' },
        { item: 'Managed, monitored, and maintained MySQL servers (replicated and standalone)' },
      ],
    },
  ],
};

export default data;
