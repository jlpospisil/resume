import React from 'react';
import { ThemeProvider, Container, Grid, Typography } from '@acme/style-guide';
import { I18nextProvider } from 'react-i18next';
import theme from './theme';
import i18n from './i18n';
import Header from './components/header/Header';
import ExperienceSection from './components/experience/ExperienceSection';
import SkillsSecton from './components/skills/SkillsSection';
import EducationSection from './components/education/EducationSection';

const App: React.FC = () => (
  <ThemeProvider theme={theme}>
    <I18nextProvider i18n={i18n}>
      <Typography
        component="div"
      >
        <Container
          id="resume"
          maxWidth="md"
          disableGutters
          data-testid="resume"
        >
          <Header
            id="resume-header"
            data-testid="resume-header"
          />

          <Container
            id="resume-content"
            component="main"
            data-testid="resume-content"
          >
            <Grid container>
              <Grid
                item
                xs={12}
                sm={7}
                className="resume-column"
              >
                <ExperienceSection />
              </Grid>

              <Grid
                item
                xs={12}
                sm={5}
                className="resume-column"
              >
                <SkillsSecton />
                <EducationSection />
              </Grid>
            </Grid>
          </Container>
        </Container>
      </Typography>
    </I18nextProvider>
  </ThemeProvider>
);

export default App;
