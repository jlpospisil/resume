import * as React from 'react';
import { Grid } from '@acme/style-guide';
import ResumeSection from '../base/ResumeSection';
import SkillsSectonItem from './SkillsSectionItem';
import { useTranslation } from 'react-i18next';
import { Skill } from '../../i18n/types';

const SkillsSecton: React.FC = () => {
  const { t } = useTranslation();
  const skills = t('skills', { returnObjects: true });

  return (
    <ResumeSection sectionName={t('headings.skills')}>
      <Grid container>
        {Array.isArray(skills) && skills.map(({ skill, rating, scale }: Skill) => (
          <SkillsSectonItem
            key={skill}
            label={skill}
            rating={rating}
            scale={scale} />
        ))}
      </Grid>
    </ResumeSection>
  );
};

export default SkillsSecton;
