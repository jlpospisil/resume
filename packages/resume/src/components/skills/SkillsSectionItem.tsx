import * as React from 'react';
import { Grid, Typography, RatingScale } from '@acme/style-guide';
import { RatingScaleProps } from "@acme/style-guide/dist/types";

export interface SkillsSectionItemProps extends RatingScaleProps {
  label?: string
}

const SkillsSectonItem: React.FC<SkillsSectionItemProps> = ({
  label,
  rating,
  scale
}: SkillsSectionItemProps) => (
  <Grid
    container
    style={{ padding: '0.25rem 0' }}
  >
    <Grid
      item
      xs={4}
    >
      <Typography variant="body1" >
        {label}
      </Typography>
    </Grid>

    <Grid
      item
      xs={8}
    >
      <RatingScale rating={rating} scale={scale} />
    </Grid>
  </Grid>
);

export default SkillsSectonItem;
