import * as React from 'react';
import { Grid, Typography } from '@acme/style-guide';
import { Props } from '@acme/style-guide/dist/types/props';
import theme from '../../theme';

export interface ResumeSectionProps extends Props {
  sectionName: string
}
const ResumeSection: React.FC<React.PropsWithChildren<ResumeSectionProps>> = ({
  children,
  sectionName,
  ...props
}: React.PropsWithChildren<ResumeSectionProps>) => (
  <Grid
    container
    className="resume--section"
  >
    <Typography
      variant="h6"
      color="primary"
      {...props}
      style={{
        marginBottom: '1rem',
        borderBottom: `2px solid ${theme.palette.primary.main}`
      }}
    >
      {sectionName}
    </Typography>

    {children}
  </Grid>
);

export default ResumeSection;
