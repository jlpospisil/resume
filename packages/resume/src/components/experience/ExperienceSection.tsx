import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Grid } from '@acme/style-guide';
import ResumeSection from '../base/ResumeSection';
import ExperienceSectionItem from './ExperienceSectionItem';
import { ResumeExperienceItem } from '../../i18n/types';

const ExperienceSection: React.FC = () => {
  const { t } = useTranslation();
  const experience = t('experience', { returnObjects: true });

  return (
    <ResumeSection sectionName={t('headings.experience')}>
      <Grid container>
        {Array.isArray(experience) && experience.map(({
          company,
          title,
          startDate,
          endDate,
          bulletItems,
        }: ResumeExperienceItem) => (
          <ExperienceSectionItem
            key={`${company}-${title}-${startDate}`}
            company={company}
            title={title}
            startDate={startDate}
            endDate={endDate}
            bulletItems={bulletItems}
          />
        ))}
      </Grid>
    </ResumeSection>
  )
};

export default ExperienceSection;
