import * as React from 'react';
import { Grid, Typography } from '@acme/style-guide';
import { useTranslation } from 'react-i18next';
import { ResumeExperienceItem } from '../../i18n/types';
import ExperienceSectionItemBulletList from './ExperienceSectionItemBulletList';

const ExperienceSectionItem: React.FC<ResumeExperienceItem> = ({
  company,
  title,
  startDate,
  endDate,
  bulletItems = []
}: ResumeExperienceItem) => {
  const { t } = useTranslation();

  return (
    <Grid
      container
      className="resume-experience--item"
    >
      <Grid
        container
        style={{ padding: '0.5rem 0' }}
      >
        <Grid item xs>
          <Typography
            variant="body1"
            component="article"
            noWrap
          >
            <Grid item style={{ fontWeight: 700 }}>
              {title}
            </Grid>
            <Grid container>
              <Grid
                item
                xs
              >
                {company}
              </Grid>
              <Grid
                item
                xs="auto"
              >
                <Typography color="primary">
                  {startDate} - {endDate ? endDate : t('labels.present')}
                </Typography>
              </Grid>
            </Grid>
          </Typography>
        </Grid>
      </Grid>

      {Array.isArray(bulletItems) && (
        <ExperienceSectionItemBulletList items={bulletItems} />
      )}
    </Grid>
  );
};

export default ExperienceSectionItem;
