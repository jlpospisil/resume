import * as React from 'react';
import { CircleIcon, Grid, Typography } from '@acme/style-guide';
import { ResumeExperienceBulletItem } from '../../i18n/types';

interface ExperienceSectionItemBulletListProps {
  items: Array<ResumeExperienceBulletItem>,
  isSubList?: boolean,
}

const ExperienceSectionItemBulletList: React.FC<ExperienceSectionItemBulletListProps> = ({
  items,
  isSubList = false,
}: ExperienceSectionItemBulletListProps) => (
  <Typography
    variant="body2"
    component="aside"
  >
    <Grid
      container
      className={`resume-experience--item-list ${ isSubList ? 'is-sub-list' : ''}`}
    >
      {items.map(({ item, children }: ResumeExperienceBulletItem) => (
        <Grid
          key={item}
          container
          className="resume-experience--item-list-item"
        >
          <Grid
            item
            xs="auto"
          >
            <CircleIcon
              color="primary"
              height="0.5rem"
              width="0.5rem"
              filled={!isSubList}
            />
          </Grid>
          <Grid
            item
            xs
            style={{ paddingLeft: '1rem' }}
          >
            {item}
          </Grid>

          {Array.isArray(children) && (
            <ExperienceSectionItemBulletList
              key={`${item}-children`}
              items={children.map(child => ({ item: child }))}
              isSubList
            />
          )}
        </Grid>
      ))}
    </Grid>
  </Typography>
);

export default ExperienceSectionItemBulletList;
