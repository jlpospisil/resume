import * as React from 'react';
import { Grid, Typography, LocationIcon } from '@acme/style-guide';
import ResumeSection from '../base/ResumeSection';
import { useTranslation } from 'react-i18next';
import theme from '../../theme';

const SkillsSecton: React.FC = () => {
  const { t } = useTranslation();

  return (
    <ResumeSection sectionName={t('headings.education')}>
      <Grid
        container
        alignContent="center"
        alignItems="center"
      >
        <Grid
          item
          xs
          style={{ paddingBottom: '1rem' }}
        >
          {t('education.degree')}
        </Grid>
        <Grid
          item
          xs={12}
        >
          <Grid
            container
            style={{ color: theme.palette.grey['700'] }}
          >
            <Grid
              item
              xs
            >
              <Grid container>
                <Grid
                  item
                  xs
                >
                  {t('education.school')}
                </Grid>
              </Grid>
              <Grid
                item
                xs={12}
                className="d-inline-flex"
                style={{ color: theme.palette.grey['500'] }}
              >
                <LocationIcon fontSize="small" />
                <Typography
                  variant="body2"
                  component="span"
                >
                  {t('education.location')}
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              xs="auto"
            >
              {t('education.graduationDate')}
              <Typography
                variant="body2"
                align="right"
                style={{ color: theme.palette.grey['500'] }}
              >
                GPA: {t('education.gpa')}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </ResumeSection>
  );
};

export default SkillsSecton;
