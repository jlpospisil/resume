import * as React from 'react';
import { Grid } from '@acme/style-guide';
import theme from '../../theme';
import ContactInformation from './ContactInformation';
import PersonalData from './PersonalData';

const Header: React.FC<React.ComponentProps<any>> = (props: any = {}) => (
  <Grid
    container
    component="header"
    {...props}
    data-testid="resume-header"
  >
    <PersonalData
      id="resume-header--personal"
      data-testid="resume-header--personal"
      style={{
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.secondary.contrastText
      }}
    />

    <ContactInformation
      id="resume-header--contact-info"
      data-testid="resume-header--contact-info"
      style={{
        backgroundColor: theme.palette.secondary.dark,
        color: theme.palette.secondary.contrastText
      }}
    />
  </Grid>
);

export default Header;
