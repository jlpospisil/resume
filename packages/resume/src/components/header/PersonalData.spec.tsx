import * as React from 'react';
import { render } from '@testing-library/react';
import PersonalData from './PersonalData';

describe('PersonalData', () => {
  it('renders', () => {
    const { getByTestId } = render(<PersonalData />);
    expect(getByTestId('resume-header--personal')).toBeInTheDocument();
  });
});
