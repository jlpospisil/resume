import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Grid,
  Typography,
  EmailIcon,
  LocationIcon,
  MobilePhoneIcon
} from '@acme/style-guide';

const ContactInformation: React.FC<React.ComponentProps<any>> = (props: any = {}) => {
  const { t } = useTranslation();

  return (
    <Grid
      {...props}
      container
      alignItems="center"
      justify="space-between"
      data-testid="resume-header--contact-info"
    >
      <Grid
        item
        className="d-inline-flex"
      >
        <EmailIcon
          color="primary"
          fontSize="small"
          className="mr-1"
        />
        <Typography variant="body2">
          {t('contactInformation.email')}
        </Typography>
      </Grid>

      <Grid
        item
        className="d-inline-flex"
      >
        <MobilePhoneIcon
          color="primary"
          fontSize="small"
          className="mr-1"
        />
        <Typography variant="body2">
          {t('contactInformation.phoneNumber')}
        </Typography>
      </Grid>

      <Grid
        item
        className="d-inline-flex hidden-xs"
      >
        <LocationIcon
          color="primary"
          fontSize="small"
          className="mr-1"
        />
        <Typography variant="body2">
          {t('contactInformation.address')}
        </Typography>
      </Grid>
    </Grid>
  );
};

export default ContactInformation;
