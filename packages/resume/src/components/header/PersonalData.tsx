import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { Avatar, Grid, Typography } from '@acme/style-guide';
import profilePicture from '../../assets/profile-picture.jpeg';
import theme from '../../theme';

const PersonalData: React.FC<React.ComponentProps<any>> = (props: any = {}) => {
  const { t } = useTranslation();

  return (
    <Grid
      {...props}
      container
      alignItems="center"
      data-testid="resume-header--personal"
    >
      <Grid
        item
        xs={12}
        sm
      >
        <Typography variant="h4">
          {t('contactInformation.name')}
        </Typography>

        <Typography
          variant="body1"
          color="primary"
        >
          {t('contactInformation.title')}
        </Typography>

        <Typography className="mt-1">
          {t('about')}
        </Typography>
      </Grid>

      <Grid
        item
        xs={12}
        sm="auto"
      >
        <Avatar
          id="resume-header--photo"
          alt={t('contactInformation.name')}
          src={profilePicture}
          style={{ border: `3px solid ${theme.palette.primary.main}` }}
        />
      </Grid>
    </Grid>
  );
};

export default PersonalData;
