import * as React from 'react';
import { render } from '@testing-library/react';
import ContactInformation from './ContactInformation';

describe('ContactInformation', () => {
  it('renders', () => {
    const { getByTestId } = render(<ContactInformation />);
    expect(getByTestId('resume-header--contact-info')).toBeInTheDocument();
  });
});
