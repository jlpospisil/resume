import * as React from 'react';
import { render } from '@testing-library/react';
import Header from './Header';

describe('Header', () => {
  it('renders', () => {
    const { getByTestId } = render(<Header />);
    expect(getByTestId('resume-header')).toBeInTheDocument();
  });

  describe('personal data', () => {
    it('renders', () => {
      const { getByTestId } = render(<Header />);
      expect(getByTestId('resume-header--personal')).toBeInTheDocument();
    });
  });

  describe('contact information', () => {
    it('renders', () => {
      const { getByTestId } = render(<Header />);
      expect(getByTestId('resume-header--contact-info')).toBeInTheDocument();
    });
  });
});
