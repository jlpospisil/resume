import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

describe('App', () => {
  it('renders', () => {
    const { getByTestId } = render(<App />);
    expect(getByTestId('app')).toBeInTheDocument();
  });

  it('renders react logo', () => {
    const { getByTestId } = render(<App />);
    expect(getByTestId('react-logo')).toBeInTheDocument();
  });

  it('renders learn react link', () => {
    const { getByText } = render(<App />);
    expect(getByText(/learn react/i)).toBeInTheDocument();
  });
});
