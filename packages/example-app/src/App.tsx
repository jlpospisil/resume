import React from 'react';
import { ReactLogo, MobilePhoneIcon, EmailIcon, LocationIcon, Button, Fab } from '@acme/style-guide';
import './App.css';

const iconSize = '2rem';

const App: React.FC = () => (
  <div
    className="App"
    data-testid="app"
  >
    <header className="App-header">
      <ReactLogo
        className="App-logo"
        width="40vmin"
        height="40vmin"
        data-testid="react-logo"
      />
    </header>

    <main className="App-content">
      <p>
        Components on this screen are being pulled from the <code>style-guide</code> package.
      </p>

      <p className="example-section">
        <MobilePhoneIcon
          htmlColor="red"
          height={iconSize}
          width={iconSize}
        />
        <EmailIcon
          htmlColor="white"
          height={iconSize}
          width={iconSize}
        />
        <LocationIcon
          htmlColor="blue"
          height={iconSize}
          width={iconSize}
        />
      </p>

      <p className="example-section">
        <Button
          endIcon={<LocationIcon htmlColor="darkOrange" />}
        >
          Primary Button
        </Button>

        <Button
          variant="outlined"
          color="secondary"
        >
          Secondary Button
        </Button>

        <Button
          color="default"
          startIcon={<MobilePhoneIcon htmlColor="red" />}
        >
          Default Button
        </Button>
      </p>

      <p className="example-fab">
        <Fab
          color="primary"
          size="medium"
        >
          <EmailIcon />
        </Fab>
      </p>
    </main>
  </div>
);

export default App;
