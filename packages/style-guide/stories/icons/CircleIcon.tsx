import * as React from 'react';
import { boolean, select, text } from '@storybook/addon-knobs';
import { CircleIcon } from '../../src/components/icons';
import { MaterialIconColors, MaterialIconFontSizes } from '../../src/types';

export default {
  title: 'Images/Icons/CircleIcon',
  component: CircleIcon,
};

export const Default = () => (
  <CircleIcon
    color={ select('color', [undefined, ...Object.values(MaterialIconColors)], undefined) }
    htmlColor={ text('htmlColor', undefined) }
    height={ text('height', undefined) }
    width={ text('width', undefined) }
    // fontSize={ select('font size', [undefined, ...Object.values(MaterialIconFontSizes)], undefined) }
    filled={ boolean('filled', false) }
  />
);

Default.story = {
  parameters: {
    jest: ['CircleIcon.spec.tsx'],
  },
};
