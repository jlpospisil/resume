import * as Icons from '../../src/components/icons';

export const AVAILABLE_ICONS = ['', ...Object.keys(Icons)];
