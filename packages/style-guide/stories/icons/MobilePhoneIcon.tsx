import * as React from 'react';
import { select, text } from '@storybook/addon-knobs';
import {CircleIcon, MobilePhoneIcon} from '../../src/components/icons';
import { MaterialIconColors, MaterialIconFontSizes } from '../../src/types';

export default {
  title: 'Images/Icons/MobilePhoneIcon',
  component: MobilePhoneIcon,
};

export const Default = () => (
  <MobilePhoneIcon
    color={ select('color', [undefined, ...Object.values(MaterialIconColors)], undefined) }
    htmlColor={ text('htmlColor', undefined) }
    height={ text('height', undefined) }
    width={ text('width', undefined) }
    fontSize={ select('font size', [undefined, ...Object.values(MaterialIconFontSizes)], undefined) }
  />
);

Default.story = {
  parameters: {
    jest: ['MobilePhoneIcon.spec.tsx'],
  },
};
