import * as React from 'react';
import { select, text } from '@storybook/addon-knobs';
import { Avatar } from '../../src/components/general';
import { DefaultTheme } from '../../src/components/base';

const avatarImageUrl = 'http://icons.iconarchive.com/icons/diversity-avatars/avatars/256/batman-icon.png';

export default {
  title: 'General/Avatar',
  component: Avatar,
};

export const Default = () => (
  <Avatar
    src={ text('source', avatarImageUrl) }
    alt={ text('alt', undefined) }
    children={ text('text', undefined) }
    variant={ select('variant', ['circle', 'rounded', 'square'], 'circle') }
    style={{
      padding: text('padding', '2px'),
      backgroundColor: text('background color', DefaultTheme.palette.secondary.light),
      border: text('border', `2px solid #000`)
    }}
  />
);

Default.story = {
  parameters: {
    jest: ['Avatar.spec.tsx'],
  },
};
