import * as React from 'react';
import { number, text } from '@storybook/addon-knobs';
import { RatingScale } from '../../src/components/general';

export default {
  title: 'General/RatingScale',
  component: RatingScale,
};

export const Default = () => (
  <RatingScale
    rating={ number('rating', 3) }
    scale={ number('scale', 5) }
    color={ text('color', undefined) }
  />
);

Default.story = {
  parameters: {
    jest: ['RatingScale.spec.tsx'],
  },
};
