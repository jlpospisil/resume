import * as React from 'react';
import { boolean, select, text } from '@storybook/addon-knobs';
import { Button } from '../../src/components/buttons';
import { ButtonColor, ButtonSize, ButtonVariant, MaterialIconColors } from '../../src/types'
import { AVAILABLE_ICONS } from '../icons/constants';
import * as Icons from '../../src/components/icons';

export default {
  title: 'Buttons/Button',
  component: Button,
};

export const Default = () => {
  const startIconName: string | undefined = select('start icon', AVAILABLE_ICONS, undefined);
  const StartIcon = Icons[startIconName];
  const startIconColor: string | undefined = select('start icon color', [undefined, ...Object.values(MaterialIconColors)], undefined);
  const startIconHtmlColor: string | undefined = text('start icon html color', undefined);
  const endIconName: string | undefined = select('end icon', AVAILABLE_ICONS, undefined);
  const EndIcon = Icons[endIconName];
  const endIconColor: string | undefined = select('end icon color', [undefined, ...Object.values(MaterialIconColors)], undefined);
  const endIconHtmlColor: string | undefined = text('end icon html color', undefined);

  return (
    <Button
      variant={select('variant', ButtonVariant, ButtonVariant.CONTAINED)}
      color={select('color', ButtonColor, ButtonColor.PRIMARY)}
      size={select('size', ButtonSize, ButtonSize.LARGE)}
      disabled={boolean('disabled', false)}
      startIcon={StartIcon && <StartIcon color={startIconColor} htmlColor={startIconHtmlColor} />}
      endIcon={EndIcon && <EndIcon color={endIconColor} htmlColor={endIconHtmlColor} />}
    >
      Button Text
    </Button>
  );
};

Default.story = {
  parameters: {
    jest: ['Button.spec.tsx'],
  },
};
