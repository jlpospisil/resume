import * as React from 'react';
import { boolean, select, text } from '@storybook/addon-knobs';
import { Fab } from '../../src/components/buttons';
import { ButtonColor, ButtonSize, FabVariant, MaterialIconColors } from '../../src/types'
import { AVAILABLE_ICONS } from '../icons/constants';
import * as Icons from '../../src/components/icons';

export default {
  title: 'Buttons/Fab',
  component: Fab,
};

export const Default = () => {
  const variant: FabVariant = select('variant', FabVariant, FabVariant.ROUND);
  const buttonColor: ButtonColor = select('color', ButtonColor, ButtonColor.PRIMARY);
  const size: ButtonSize = select('size', ButtonSize, ButtonSize.LARGE);
  const disabled: boolean = boolean('disabled', false);
  const iconName: string | undefined = select('icon', AVAILABLE_ICONS, AVAILABLE_ICONS[1]);
  const Icon = Icons[iconName];
  const iconColor: string | undefined = select('icon color', [undefined, ...Object.values(MaterialIconColors)], undefined);
  const iconHtmlColor: string | undefined = text('icon html color', undefined);

  return (
    <Fab
      variant={variant}
      color={buttonColor}
      size={size}
      disabled={disabled}
    >
      {text('button text', undefined)}
      {Icon && <Icon color={iconColor} htmlColor={iconHtmlColor} />}
    </Fab>
  );
};

Default.story = {
  parameters: {
    jest: ['Fab.spec.tsx'],
  },
};
