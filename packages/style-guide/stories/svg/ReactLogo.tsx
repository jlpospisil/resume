import * as React from 'react';
import { text } from '@storybook/addon-knobs';
import { ReactLogo } from '../../src/components/svg';

export default {
  title: 'Images/Logos/ReactLogo',
  component: ReactLogo,
};

export const Default = () => (
  <ReactLogo
    color={ text('color', '#61DAFB') }
    height={ text('height', '40vmin') }
    width={ text('width', '40vmin') }
  />
);

Default.story = {
  parameters: {
    jest: ['ReactLogo.spec.tsx'],
  },
};
