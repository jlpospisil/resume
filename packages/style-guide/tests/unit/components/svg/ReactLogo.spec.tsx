import * as React from 'react';
import { render } from '@testing-library/react';
import { ReactLogo, defaultColor, defaultHeight, defaultWidth } from '../../../../src/components/svg/ReactLogo';

describe('ReactLogo', () => {
  it('renders', () => {
    const { getByTestId } = render(<ReactLogo />);
    expect(getByTestId('react-logo')).toBeTruthy();
  });

  describe('props', () => {
    describe('color', () => {
      const params = [undefined, 'red', '#FFFFFF', 'rgb(0, 0, 0)', 'rgba(175, 175, 175, 0.3)'];
      it.each(params)('provides the expected fill color given %p', color => {
        const { getByTestId } = render(<ReactLogo color={color} />);
        const attributeValue = getByTestId('react-logo--fill-color').getAttribute('fill');
        expect(attributeValue).toEqual(color || defaultColor);
      })
    });

    describe('height', () => {
      const params = [undefined, '50px', '40vmin', '10rem', '8em', '10%'];
      it.each(params)('provides the expected height given %p', height => {
        const { getByTestId } = render(<ReactLogo height={height} />);
        const attributeValue = getByTestId('react-logo').getAttribute('height');
        expect(attributeValue).toEqual(height || defaultHeight);
      })
    });

    describe('width', () => {
      const params = [undefined, '50px', '40vmin', '10rem', '8em', '10%'];
      it.each(params)('provides the expected width given %p', width => {
        const { getByTestId } = render(<ReactLogo width={width} />);
        const attributeValue = getByTestId('react-logo').getAttribute('width');
        expect(attributeValue).toEqual(width || defaultWidth);
      })
    });

    describe('className', () => {
      const params = [undefined, 'additional-css-class'];
      it.each(params)('provides the expected className given %p', className => {
        const { getByTestId } = render(<ReactLogo className={className} />);
        const attributeValue = getByTestId('react-logo').getAttribute('class');
        expect(attributeValue).toEqual(className || null);
      })
    });
  });
});
