import { addDecorator } from '@storybook/react'
import { withA11y } from '@storybook/addon-a11y';
import { withKnobs } from '@storybook/addon-knobs';
import { withTests } from '@storybook/addon-jest';
import { muiTheme } from 'storybook-addon-material-ui';
import { DefaultTheme } from '../src/components/base';
import results from 'tests/results/jest/results.json';

addDecorator(withA11y);
addDecorator(withKnobs);
addDecorator(withTests({ results }));
addDecorator(muiTheme([DefaultTheme]));
