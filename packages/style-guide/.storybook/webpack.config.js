const path = require('path');

module.exports = {
  resolve: {
    modules: [
      path.resolve(__dirname, '..'), // Allows importing jest results from outside src directory for addon-jest
      'node_modules'
    ]
  }
};
