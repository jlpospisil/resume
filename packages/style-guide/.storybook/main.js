const path = require('path');

module.exports = {
  stories: ['../stories/**/*.tsx'],
  addons: [
    '@storybook/addon-a11y/register',
    '@storybook/addon-actions/register',
    {
      name: '@storybook/addon-docs',
      options: { configureJSX: true }
    },
    '@storybook/addon-jest/register',
    '@storybook/addon-knobs',
    '@storybook/addon-links',
    {
      name: '@storybook/addon-storysource',
      options: {
        rule: {
          test: [/\.tsx?$/],
          include: [path.resolve(__dirname, '../stories')],
        },
        loaderOptions: {
          prettierConfig: { printWidth: 80, singleQuote: false },
        },
      },
    },
    {
      name: '@storybook/preset-create-react-app',
      options: {
        tsDocgenLoaderOptions: {
          tsconfigPath: path.resolve(__dirname, '../tsconfig.json')
        },
      },
    },
  ],
  webpackFinal: async config => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      use: [
        { loader: require.resolve('awesome-typescript-loader') },
        { loader: require.resolve('react-docgen-typescript-loader') },
      ],
    });
    config.module.rules.push({
      test: /\/stories\/.*\.(ts|tsx)$/,
      use: [
        {
          loader: require.resolve('@storybook/source-loader'),
          options: { parser: 'typescript' },
        }
      ],
    });
    config.resolve.extensions.push('.ts', '.tsx');
    return config;
  }
};
