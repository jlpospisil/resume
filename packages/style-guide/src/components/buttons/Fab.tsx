import * as React from 'react';
import { FabVariant, ButtonColor } from '../../types'
import { Fab as MaterialFab, FabProps } from '@material-ui/core';

export const Fab: React.FC<React.PropsWithChildren<FabProps>> = ({
  children,
  variant = FabVariant.ROUND,
  color = ButtonColor.PRIMARY,
  ...props
}: React.PropsWithChildren<FabProps>) => (
  <MaterialFab
    color={color}
    {...props}
  >
    {children}
  </MaterialFab>
);

export default Fab;
