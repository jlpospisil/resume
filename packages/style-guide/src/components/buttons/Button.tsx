import * as React from 'react';
import MaterialButton, { ButtonProps } from '@material-ui/core/Button';
import { ButtonColor, ButtonVariant } from '../../types'

export const Button: React.FC<React.PropsWithChildren<ButtonProps>> = ({
  children,
  variant = ButtonVariant.CONTAINED,
  color = ButtonColor.PRIMARY,
  ...props
}: React.PropsWithChildren<ButtonProps>) => (
  <MaterialButton
    variant={variant}
    color={color}
    {...props}
  >
    {children}
  </MaterialButton>
);

// export class Button extends React.Component<React.PropsWithChildren<ButtonProps>, {}> {
//   render () {
//     return (
//       <MaterialButton
//         variant={this.props.variant || ButtonVariant.CONTAINED}
//         color={this.props.color || ButtonColor.PRIMARY}
//         {...this.props}
//       >
//         {this.props.children}
//       </MaterialButton>
//     )
//   }
// }

export default Button;
