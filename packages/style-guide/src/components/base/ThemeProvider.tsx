import * as React from 'react';
import { createMuiTheme, ThemeProvider as MaterialThemeProvider } from '@material-ui/core';
import { ThemeProviderProps } from '../../types';
import primary from '@material-ui/core/colors/blue';
import secondary from '@material-ui/core/colors/blueGrey';
import grey from '@material-ui/core/colors/grey';

export const DefaultTheme = createMuiTheme({
  palette: {
    primary,
    secondary: {
      light: secondary.A400,
      main: secondary["800"],
      dark: secondary["900"],
      contrastText: grey.A100
    }
  }
});

export const ThemeProvider: React.FC<React.PropsWithChildren<ThemeProviderProps>> = ({
 children,
  theme = DefaultTheme,
 ...props
}: React.PropsWithChildren<ThemeProviderProps>) => (
  <MaterialThemeProvider
    theme={theme}
    {...props}
  >
    {children}
  </MaterialThemeProvider>
);
