import * as React from 'react';
import MaterialContainer from '@material-ui/core/Container';
import { ContainerProps } from '../../types';

export const Container: React.FC<React.PropsWithChildren<ContainerProps>> = ({
   children,
   ...props
 }: React.PropsWithChildren<ContainerProps>) => (
  <MaterialContainer {...props}>
    {children}
  </MaterialContainer>
);

// export class Container extends React.Component<React.PropsWithChildren<ContainerProps>, {}> {
//   render () {
//     return (
//       <MaterialContainer {...this.props}>
//         {this.props.children}
//       </MaterialContainer>
//     )
//   }
// }

export default Container;
