import * as React from 'react';
import MaterialGrid from '@material-ui/core/Grid';
import { GridProps } from '../../types';

export const Grid: React.FC<React.PropsWithChildren<GridProps>> = ({
   children,
   ...props
 }: React.PropsWithChildren<GridProps>) => (
  <MaterialGrid {...props}>
    {children}
  </MaterialGrid>
);

export default Grid;
