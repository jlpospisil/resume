import * as React from 'react';
import { LocationOn } from '@material-ui/icons';
import { IconProps } from '../../types';

export const LocationIcon: React.FC<IconProps> = ({
  height,
  width,
  ...props
}: IconProps) => (
  <LocationOn
    style={{
      height,
      width,
    }}
    {...props}
  />
);

export default LocationIcon;
