import * as React from 'react';
import { useTheme } from '@material-ui/core/styles';
import { FillableIconProps } from '../../types';
import { DefaultTheme } from '../base';
import { findMaterialIconColorByName } from '../../utils';

export const defaultColor = DefaultTheme.palette.text.primary;
export const defaultHeight = '1.25rem';
export const defaultWidth = defaultHeight;

export const CircleIcon: React.FC<FillableIconProps> = ({
  color,
  htmlColor = defaultColor,
  height = defaultHeight,
  width= defaultWidth,
  filled = false,
  style,
  ...props
}: FillableIconProps) => {
  const theme = useTheme();
  const finalColor: string | undefined = findMaterialIconColorByName(theme, color) || htmlColor || defaultColor;

  return (
    <svg
      width={width}
      height={height}
      focusable="false"
      viewBox="0 0 24 24"
      aria-hidden="true"
      role="presentation"
      style={style}
    >
      <circle
        stroke={finalColor}
        fill={filled ? finalColor : 'none'}
        r={filled ? 12 : 10.5}
        cx="12"
        cy="12"
        strokeWidth={filled ? 0 : 3}
      />
    </svg>
  );
};
