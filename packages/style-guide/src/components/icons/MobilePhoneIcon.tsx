import * as React from 'react';
import { PhoneAndroid } from '@material-ui/icons';
import { IconProps } from '../../types';

export const MobilePhoneIcon: React.FC<IconProps> = ({
  height,
  width,
  ...props
}: IconProps) => (
  <PhoneAndroid
    style={{
      height,
      width,
    }}
    {...props}
  />
);

export default MobilePhoneIcon;
