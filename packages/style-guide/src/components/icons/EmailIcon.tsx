import * as React from 'react';
import { Email } from '@material-ui/icons';
import { IconProps } from '../../types';

export const EmailIcon: React.FC<IconProps> = ({
  height,
  width,
  ...props
}: IconProps) => (
  <Email
    style={{
      height,
      width,
    }}
    {...props}
  />
);

export default EmailIcon;
