import * as React from 'react';
import { Grid } from '../layout';
import { CircleIcon } from '../icons';
import { RatingScaleProps } from '../../types';
import { useTheme } from '@material-ui/core/styles';
import { Theme } from '@material-ui/core';
import { findMaterialIconColorByName } from '../../utils';

export const RatingScale: React.FC<RatingScaleProps> = ({
  rating = 0,
  scale = 5,
  color,
  ...props
}: RatingScaleProps) => {
  const theme: Theme = useTheme();
  const iconColor = findMaterialIconColorByName(theme, color) || color || theme.palette.primary.main;
  const sectionKeyId: number = Math.floor(Math.random() * 1000);

  return (
    <Grid
      container
      justify="space-evenly"
      {...props}
    >
      {[...Array(scale).keys()].map(index => (
        <Grid
          item
          key={`skills-section-${sectionKeyId}-${index}`}
        >
          <CircleIcon
            htmlColor={iconColor}
            filled={index < rating}
          />
        </Grid>
      ))}
    </Grid>
  );
};
