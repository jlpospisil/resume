import * as React from "react";
import { Avatar as MaterialAvatar, AvatarProps } from '@material-ui/core';

export const Avatar: React.FC<React.PropsWithChildren<AvatarProps>> = (props: React.PropsWithChildren<AvatarProps>) => (
  <MaterialAvatar
    {...props}
  />
);
