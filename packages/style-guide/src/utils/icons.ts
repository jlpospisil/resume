import { Theme } from '@material-ui/core';
import { MaterialIconColors } from '../types';

export const findMaterialIconColorByName = (theme: Theme, colorName: string | undefined): string | undefined => {
  if (colorName) {
    const colorValue: string | undefined = Object.values(MaterialIconColors)
      .find(color => color === colorName);

    switch (colorValue) {
      case MaterialIconColors.INHERIT:
        return colorName;
      case MaterialIconColors.DISABLED:
        return theme.palette.action.disabled;
      case MaterialIconColors.ACTION:
        return theme.palette.action.active;
      case MaterialIconColors.PRIMARY:
      case MaterialIconColors.SECONDARY:
      case MaterialIconColors.ERROR:
        return theme.palette[colorValue].main;
    }
  }

  return undefined;
};
