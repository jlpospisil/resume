import { Props } from './props';

export interface SvgProps extends Props {
  color?: string,
  height?: string,
  width?: string
}
