import { Props } from './props';

export interface RatingScaleProps extends Props {
  rating?: number,
  scale?: number,
  color?: string,
}
