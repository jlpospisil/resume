import { ButtonProps as Props } from '@material-ui/core';

export interface ButtonProps extends Props {}

export enum ButtonVariant {
  TEXT = 'text',
  OUTLINED = 'outlined',
  CONTAINED = 'contained'
}

export enum ButtonColor {
  DEFAULT = 'default',
  PRIMARY = 'primary',
  SECONDARY = 'secondary'
}

export enum ButtonSize {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large'
}

export enum FabVariant {
  ROUND = 'round',
  EXTENDED = 'extended'
}
