import { Theme } from '@material-ui/core';

export interface ThemeProviderProps {
  theme?: Partial<Theme> | ((outerTheme: Theme) => Theme);
}
