import { SvgIconProps as MaterialIconProps } from '@material-ui/core';

export enum MaterialIconColors {
  INHERIT = 'inherit',
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  ACTION = 'action',
  ERROR = 'error',
  DISABLED = 'disabled'
}

export enum MaterialIconFontSizes {
  DEFAULT = 'default',
  INHERIT = 'inherit',
  SMALL = 'small',
  LARGE = 'large'
}

export interface IconProps extends MaterialIconProps {
  iconColor?: string,
  height?: string,
  width?: string
}

export interface FillableIconProps extends IconProps {
  filled?: boolean
}
