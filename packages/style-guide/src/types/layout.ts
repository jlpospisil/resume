import {
  ContainerProps as MaterialContainerProps,
  GridProps as MaterialGridProps,
} from '@material-ui/core';

export interface ContainerProps extends MaterialContainerProps {}
export interface GridProps extends MaterialGridProps {}
